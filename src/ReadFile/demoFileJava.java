package ReadFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class demoFileJava {

    public demoFileJava() throws FileNotFoundException {
        File fl = new File("listNama.txt");
        Scanner scanFile = new Scanner(fl);

        while (scanFile.hasNext()) {
            String nama = scanFile.nextLine();
            System.out.println(nama);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        new demoFileJava();
    }
}
