package ProsesFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class prosesFileDemo {
    public prosesFileDemo() throws FileNotFoundException {
        File fl = new File("historyPelanggan.txt");
        PrintWriter pw = new PrintWriter(fl);
        Scanner scanFile = new Scanner(fl);

        if( fl.exists() ) {
            System.out.println("Sukses membuat file dengan nama historyPelanggan.txt");
            pw.println("Anton#10");
            pw.println("Jarwo#20");
            pw.println("Ronald#23");
            pw.println("Daniel#30");
            pw.println("Pedro#30");
            pw.close();
        } else {
            System.out.println("gagal membuat file");
        }

        String [] nama = new String[10];
        int [] umur = new int[10];
        int index = 0;
        String temp;

        while (scanFile.hasNext()) {
            temp = scanFile.nextLine();
            String [] pemisah = temp.split("#");

            nama[index] = pemisah[0];
            umur[index] = Integer.parseInt(pemisah[1]);
            index++;
        }

        System.out.println("\n\nPelanggan dari Toko Saya");
        System.out.println("========================");
        for (int i = 0; i < 5; i++) {
            System.out.println(nama[i]+", umur "+umur[i]);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        new prosesFileDemo();
    }
}
