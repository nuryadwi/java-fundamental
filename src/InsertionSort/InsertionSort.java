package InsertionSort;

public class InsertionSort {
    public InsertionSort() {
        int [] number = {8,2,1,6,4};
        int k, l, tempNumber = 0;

        System.out.println("Angka sebelum di sorting");
        System.out.println("=========================");
        for (int j = 0; j < number.length; j++)
        {
            System.out.print(number[j]+" ");
        }
        //insertion sort algoritma
        for (k = 0; k < number.length; k++)
        {
            tempNumber = number[k];

            for (l = k-1; (l >= 0 ) && (number[l] > tempNumber); l--)
            {
                number[l+1] = number[l];
            }
            number[l+1] = tempNumber;
        }
        System.out.println("\n\nAngka setelah di sorting");
        System.out.println("========================");
        for (int i = 0; i < number.length; i++)
        {
            System.out.print(number[i]+" ");
        }
    }

    public static void main(String[] args) {
        new InsertionSort();
    }
}
