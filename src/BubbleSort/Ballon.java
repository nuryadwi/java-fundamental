package BubbleSort;

public class Ballon
{
    public Ballon()
    {
        String [] listName = {"Doni","Asep","Rendi","Damar","Dona"};
        String tempName;

        System.out.println("Daftar nama sebelum di sorting");
        System.out.println("==============================");
        for (int i = 0; i < listName.length; i++) {
            System.out.println((i+1)+". "+listName[i]);
        }
        //bubble sort
        for (int i = 1; i < listName.length; i++) {
            for (int j = 0; j < listName.length-i; j++) {
                //change logic
                if (listName[j].compareTo(listName[j+1]) > 0 ) {
                    tempName = listName[j];
                    listName[j] = listName[j+1];
                    listName[j+1] = tempName;
                }
            }
        }
        System.out.println("\n\nDaftar nama setelah di sorting - asc");
        System.out.println("==============================");
        for (int i = 0; i < listName.length; i++) {
            System.out.println((i+1)+". "+ listName[i]);
        }
    }

    public static void main(String[] args)
    {
        new Ballon();
    }
}
