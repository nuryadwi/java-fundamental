import java.util.Scanner;

public class StatementPemilihan {
    public StatementPemilihan() {
        Scanner scan = new Scanner(System.in);
        int nilaiUjian;

        //if statement
        System.out.print("Masukkan nilai ujian: ");
        nilaiUjian = scan.nextInt();
//        if( nilaiUjian < 75 ){
//            System.out.println("Grade nilai anda C");
//        }
        if( nilaiUjian > 75 && nilaiUjian < 100) {
            System.out.println("Grade nilai anda A");
        }else if( nilaiUjian > 65 && nilaiUjian < 74) {
            System.out.println("Grade nilai anda B");
        } else if( nilaiUjian > 55 && nilaiUjian < 65) {
            System.out.println("Grade nilai anda C");
        } else {
            System.out.println("Anda tidak lulus ujian");
        }
    }
    public static void main(String[] args) {
        new StatementPemilihan();
    }
}
