public class Sorting1 {
    public Sorting1()
    {
        String[] listName = {"Budi","Angga","Andi","Caca","Bagus","Erwin"};
        int[] listNumber = {6,7,2,9,1,5};

        System.out.println("List Nama dan angka sebelum diurutkan");
        System.out.println("===========================");

        for (int i = 0; i < listNumber.length; i++) {
            System.out.println(listName[i]+" "+" memiliki angka "+listNumber[i]);
        }
        //pengurutan angka
        int tempNumber = 0;
        String tempWords;
        for (int i = 0; i < listNumber.length; i++)
        {
            for (int j = 0; j < listNumber.length-1; j++)
            {
                //statement change place
                if (listName[j].compareTo(listName[j+1]) > 0) {
                    //string sort
                    tempWords = listName[j];
                    listName[j] = listName[j+1];
                    listName[j+1] = tempWords;
                    //int sort
                    tempNumber = listNumber[j];
                    listNumber[j] = listNumber[j+1];
                    listNumber[j+1] = tempNumber;
                }
            }
        }

        System.out.println("\n\nNama dan setelah diurutkan");
        System.out.println("\n\n==========================");

        for (int i = 0; i < listNumber.length; i++) {
            System.out.println(listName[i]+" "+" memiliki angka "+listNumber[i]);
        }
    }

    public static void main(String[] args)
    {
        new Sorting1();
    }
}
