import java.util.Scanner;

public class SwitchExample {
    public SwitchExample(){
        Scanner scan = new Scanner(System.in);
        int pilihan;

        System.out.println("Menu Registrasi");
        System.out.println("===============");
        System.out.println("1. Daftar");
        System.out.println("2. Login");
        System.out.println("3. Exit");
        System.out.println("Masukkan pilihan menu : ");
        pilihan = scan.nextInt();

        switch (pilihan) {
            case 1:
                System.out.println("Ini adalah menu daftar");
                break;
            case 2:
                System.out.println("Ini adalah menu Login");
                break;
            case 3:
                System.out.println("Ini adalah menu Exit");
                break;
            default:
                System.out.println("Input menu tidak ada dalam pilihan menu");
                break;
        }
    }
    public static void main(String[] args){
        new SwitchExample();
    }
}
