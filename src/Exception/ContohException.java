package Exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ContohException {

    public ContohException() {
        Scanner scan = new Scanner(System.in);
        int input = 0;
        try {
            System.out.println("Masukkan angka: ");
            input = scan.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("input yang dimasukkan salah harus angka bos!");
            scan.nextLine();
        }

        if (input != 10) {
            System.out.println("karena salah, maka nilai dari input yang dimasukkan adalah "+input);
        } else {
            System.out.println("karena benar, maka nilai dari input yang dimasukkan adalah "+input);
        }
    }

    public static void main(String[] args) {
        new ContohException();
    }
}
