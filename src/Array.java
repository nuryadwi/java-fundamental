public class Array {
    public Array() {
       //contoh deklarasi array
//      String[] daftarnama = new String[5];
        String[] daftarnama = {"Angga","Andi","Caca","Bagus","Erwin"};
        int kapasitas = daftarnama.length;
        System.out.println("Besar Kapasitas array diatasa adalah sebesar "+kapasitas);
        System.out.println("\nDaftar nama adalah sebagai berikut ");
        for (int a=0; a < kapasitas; a++){
            System.out.println("Nama ke-"+(a+1)+" adalah "+daftarnama[a]);
        }
        //do-while
        System.out.println("\nDaftar nama adalah sebagai berikut dalam do-while ");
        int batas=0;
        do {
            System.out.println("Nama ke-"+(batas+1)+" adalah "+daftarnama[batas]);
            batas++;
        }while (batas < daftarnama.length);
        //while
        int b=0;
        System.out.println("\nDaftar nama adalah sebagai berikut dalam while ");
        while (b < daftarnama.length) {
            System.out.println("Nama ke-"+(b+1)+" adalah "+daftarnama[b]);
            b++;
        }

    }

    public static void main(String[] args) {
        new Array();
    }
}
