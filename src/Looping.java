public class Looping {
    public Looping(){
        int nilai = 5;
        //for
        /* for (int i = 0; i < nilai; i++) {
            System.out.println("Putaran ke "+(i+1)+" nilai vaiabel i = "+i);
        } */
        //while
        /*
        while ( batasAwal < nilai) {
            System.out.println("pada putaran ke "+(batasAwal+1)+" While masih berjalan dengan nilai = "+batasAwal);
            batasAwal++;
        } */
        //do-while
        int batasAwal = 0;
        System.out.println("Hasil dari statement do-while");
        do {
            System.out.println("pada putaran ke "+(batasAwal+1)+" While masih berjalan dengan nilai = "+batasAwal);
            batasAwal++;
        } while (batasAwal < nilai);
    }

    public static void main(String[] args) {
        new Looping();
    }
}
