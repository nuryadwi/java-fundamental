package method;

public class MethodClass
{
    //method
    public void cetakPersegi()
    {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print("* ");
            }
            System.out.println("");
        }
    }
    //method return value
    public int hitungHasil(int a, int b, int c)
    {
        a = b+c;

        return a;
    }
    public MethodClass() //constructor
    {
        /*
        method header
            access modifier
            return type
            nama method
            parameter
        method body
            argumen method
        membuat method di luar dari constructor
        * */

        // contoh method void tidak mengembalikan nilai apa2
        cetakPersegi();
        // contoh method return value
        int hasilMethodReturnValue = hitungHasil(0,5,5);
        System.out.println("hasil method return value = "+hasilMethodReturnValue);
    }

    public static void main(String[] args)
    {
        new MethodClass();
    }
}
