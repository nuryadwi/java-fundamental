package ArrayOfObject;

public class ArrayOfObject {
    public ArrayOfObject()
    {
        Mobil[] listMobil = new Mobil[3];
        listMobil[0] = new Mobil();
        listMobil[0].setNamaMobil("Avanza");
        listMobil[0].setKecepatanMobil(100);

        listMobil[1] = new Mobil();
        listMobil[1].setNamaMobil("Jazz");
        listMobil[1].setKecepatanMobil(120);

        listMobil[2] = new Mobil();
        listMobil[2].setNamaMobil("Yaris");
        listMobil[2].setKecepatanMobil(130);

        for (int i = 0; i < listMobil.length; i++) {
            System.out.println("Nama ArrayOfObject.Mobil ke-"+(i+1)+" adalah"+listMobil[i].getNamaMobil()+" , kecepatan :"+listMobil[i].getKecepatanMobil());
        }
    }

    public static void main(String[] args)
    {
        new ArrayOfObject();
    }
}
