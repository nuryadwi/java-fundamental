package ArrayOfObject;

public class Mobil {
    String namaMobil;
    int kecepatanMobil;

    //setter & getter
    public String getNamaMobil() {
        return namaMobil;
    }
    public void setNamaMobil(String namaMobil) {
        this.namaMobil = namaMobil;
    }
    public int getKecepatanMobil() {
        return kecepatanMobil;
    }
    public void setKecepatanMobil(int kecepatanMobil) {
        this.kecepatanMobil = kecepatanMobil;
    }
    //behavior
    public void aktivitasMobil()
    {
        System.out.println("ArrayOfObject.Mobil ini bisa untuk balapan..");
    }
}
