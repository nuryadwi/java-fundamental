import java.util.Arrays;

public class SortingArray {
    public SortingArray() {
        String[] listName = {"Angga","Andi","Caca","Bagus","Erwin"};
        int[] listNumber = {7,2,9,1,5};

        System.out.println("Daftar Nama sebelum diurutikan");
        for (int i = 0; i < listNumber.length; i++) {
            System.out.println((i+1)+". "+listNumber[i]);
        }
        Arrays.sort(listNumber);

        System.out.println("\nDaftar Nama stelah diurutkan");
        for (int i = 0; i < listNumber.length; i++) {
            System.out.println((i+1)+". "+listNumber[i]);
        }
    }

    public static void main(String[] args) {
        new SortingArray();
    }
}
