import java.util.Scanner;

public class MyBodyInformation {
    public MyBodyInformation() {
        Scanner scan = new Scanner(System.in);
        String namaPengguna;
        int tinggiPengguna;
        int selisihTinggi;
        double beratPengguna;
        double selisihBerat;

        System.out.print("Masukkan nama : ");
        namaPengguna = scan.nextLine();
        System.out.print("Masukkan tinggai : ");
        tinggiPengguna = scan.nextInt();
        System.out.print("Massukan berat : ");
        beratPengguna = scan.nextDouble();

        System.out.println("\n\n Body Information");
        System.out.println("Nama Pengguna program "+namaPengguna);
        System.out.println("Tinggi Pengguna Program "+tinggiPengguna);
        System.out.println("Berat Pengguna Program "+beratPengguna);

        System.out.println("\n\n Setelah beberapa bulan, informasi atas berat badan adalah sebagai berikut: ");
        System.out.println("======================================================================");
        System.out.println("Nama Pengguna program"+namaPengguna);
        System.out.println("Tinggi pengguna program ="+(2*tinggiPengguna));
        System.out.println("Berat pengguna program ="+(3*beratPengguna));

        selisihTinggi = (2*tinggiPengguna)-tinggiPengguna;
        selisihBerat = (3*beratPengguna)-beratPengguna;
        System.out.println("Selisih tinggi semula dengan sekarang adalah "+selisihTinggi);
        System.out.println("Selisih berat semula dengan sekarang adalah "+selisihBerat);

    }
    public static void main(String[] args) {
        new MyBodyInformation();
    }
}
