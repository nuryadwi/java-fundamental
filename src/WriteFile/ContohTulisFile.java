package WriteFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ContohTulisFile {

    public ContohTulisFile() throws FileNotFoundException {
        File fl = new File("absen.txt");
        PrintWriter pw = new PrintWriter(fl);
        //cek file ada atau tidak
        if (fl.exists() ) {
            System.out.println("sukses membuat file");

            pw.println("Angga");
            pw.println("Satrio");
            pw.println("Bagus");
            pw.println("Caca");
            pw.close();
        } else {
            System.out.println("Gagal buat file");
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        new ContohTulisFile();
    }
}
